var map = L.map('main_map').setView([6.1906914, -75.5943078], 13);//medellin

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibWlndWVsLWphdmVsYSIsImEiOiJja3YzOHdjYWwxZGw0Mm5xMW11MnJlaWxvIn0.vYELDgH0Vy7tc6q5gYScNA'
}).addTo(map);

$.ajax({
    dataType: "json",
    url:"api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici)
        {
            L.marker(bici.ubicacion,{title: bici.id}).addTo(map)
        })
    }
})