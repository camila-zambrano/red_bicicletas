var usuario = require('../models/usuario')

module.exports = {
    list: function(req, res, next){
        usuario.find({},(err, usuarios) =>{
            res.render('usuarios/index', {usuarios:usuarios})
        })
    },
    update_get: function(req, res, next){
        usuario.findById(req.params.id, function(err,usuario){
            res.render('usuarios/update', {erros:{},usuario:usuario})
        })
    },
    update: function(req,res,next){
        var update_values =  {nombre: req.body.nombre};
        usuario.findByIdAndUpdate(req.params.id, update_values, function(err,usuario1){
            if(err){
                console.log(err);
                res.render('usuarios/update',{errors: err.errors, usuario:new usuario({nombre: req.body.nombre, email: req.body.email})})
            }else{
                res.redirect('/usuarios')
                return
            }
        })
    },
    create_get: function(req, res, next){
        res.render('usuarios/create', {errors:{}, usuario:new usuario()})

    },
    create: function(req, res, next){
        if(req.body.password != req.body.confirm_password){
            res.render('usuarios/create', {errors:{confirm_password: {message:'No coinciden las conrtaseñas'}}, 
            usuario: new usuario({nombre:  req.body.nombre, email: req.body.email})
        })
            return 
        }
        usuario.create({nombre: req.body.nombre, email: req.body.email, password: req.body.password}, function(err, nuevousuario){
            if(err){
                res.render('usuarios/create', {errors:err.errors,usuario: new usuario({nombre:  req.body.nombre, email: req.body.email})})
            }else{
                nuevousuario.enviar_email_bienvenida();
                res.redirect('/usuarios')
            }
        });
    },

    delete: function(req, res,next){
        usuario.findByIdAndDelete(req.body.id, function(err){
            if(err)
                next(err)
            else
                res.redirect('/usuarios')
                
        })
    }
}