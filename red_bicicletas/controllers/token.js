var Usuario = require('../models/usuario')
var Token = require('../models/token')

module.exports = {
    confirmationGet : function(req,res,next){
        Token.findOne({token: req.params.token}, function (err, token1){
            
            if(!token1){
                
                return res.status(400).send({type:'no-verified', msg:'no encontramos un usaurio con este token, quizas haya expirado y debas solicitar uno nuevo'})
            }
            console.log(token1._userId)
            Usuario.findById(token1._userId,function(err,usuario){
                console.log("entro")
                if(!usuario) return res.status(400).send({msg:'no encontramos un usaurio con este token'})
                usuario.verificado = true
                usuario.save(function(err){
                    if(err){return res.status(500).send({msg:err.message})}
                    res.redirect('/')
                })
            })
        })
    }
}