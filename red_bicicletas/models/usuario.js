var mongoose = require('mongoose');
var schema = mongoose.Schema;
var Reserva= require('./reserva')
const saltRounds=10;
const bcrypt = require('bcrypt')
const uniqueValidator = require('mongoose-unique-validator')
const crypto = require('crypto')
const Token = require('../models/token');
const mailer = require('../mailer/mailer')

const validateEmail = function(email){
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
}

var usuarioschema = new schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'el nombre es obligatorio']
    },
    email:{
        type: String,
        trim: true,
        required: [true, 'el email es obligatorio'],  
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'porfavor, ingrese un email valido'],
        match:[/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'la clave es obligatorio']

    },
    paswordREsettoken: String,
    paswordREsettokenExpires: Date,
    verificado:{
        type: Boolean,
        default: false
    }

});


usuarioschema.plugin(uniqueValidator, {message: 'el {PATH} ya existe con otro usuario.'});

usuarioschema.pre('save',function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds)
    }
    next()
})

usuarioschema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password)
}

usuarioschema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario : this._id, bicicleta: biciId, desde: desde, hasta: hasta })
    console.log(reserva)
    reserva.save(cb)
}


usuarioschema.methods.enviar_email_bienvenida  = function(cb){
    const token= new  Token ({_userId: this.id,token: crypto.randomBytes(16).toString('hex')})
    const email_destination = this.email;
    token.save(function(err){
        if(err){ return console.log(err.message)}

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'verificacion de cuenta',
            text:'hola, \n\n' + 'por favor, para verificar su cuenta haga click en este link: \n' + 'http://localhost:5000' + '\/token/confirmation\/' + token.token + '.\n'
        }
        mailer.sendMail(mailOptions, function(err){
            if(err){return console.log(err.message)}
            console.log('se ha enviado un email de bienvenida a: '+ email_destination)
        })
    })

}
usuarioschema.methods.resetpassword = function(cb){
    const token = new Token({_userId:this.id,token:crypto.randomBytes(16).toString('hex')})
    const email_destination = this.email
    token.save(function(err){
        if(err){return cb(err)}
        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'verificacion de cuenta',
            text:'hola, \n\n' + 'por favor, para resetear su cuenta haga click en este link: \n' + 'http://localhost:5000' + '\/resetpassword\/' + token.token + '.\n'
        }
        mailer.sendMail(mailOptions, function(err){
            if(err){return cb(err)}
            console.log('se ha enviado un email para resetear el password a: '+ email_destination)
        })
    })
}


module.exports = mongoose.model('usuario',usuarioschema)